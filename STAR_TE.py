# STAR > TEtranscript

configfile: "configTEOran.yaml"

rule all:
    input:
         expand("tecount/{sample}.cntTable", sample=config["sample"])

# STAR
rule star_pe:
    input:
        fq1 = "data/{sample}_R1.fastq",
        fq2 = "data/{sample}_R2.fastq"
    output:
        bam="/data/scratch/yaochung41/kap1/star/pe/{sample}/Aligned.sortedByCoord.out.bam"
    log:
        "logs/star/pe/{sample}.log"
    params:
        index=config["star_index"],
        extra="--outSAMtype BAM SortedByCoordinate --outFilterMultimapNmax 100 --winAnchorMultimapNmax 100"
    threads: 16
    wrapper:
        "0.74.0/bio/star/align"

# TEtranscript
rule TEcount:
    input:
        bam="/data/scratch/yaochung41/kap1/star/pe/{sample}/Aligned.sortedByCoord.out.bam",
        gtf=config["gtf"], #/data/scratch/yaochung41/gtf_file/hg38.ensGene.gtf
        rmsk=config["rmsk"] #/data/scratch/yaochung41/gtf_file/hg38_rmsk_TE.gtf.ind
    output:
        "tecount/{sample}.cntTable"
    conda:
        "TEcount.yml"
    shell:
        "TEcount -b {input.bam} --GTF {input.gtf} --TE {input.rmsk} "
        "--sortByPos --project tecount/{wildcards.sample}"
