# scbn scaling factor, and do scaling to gene count
scaling_factor <- function(df, colIndexStart.A, colIndexStart.B) {
	# colnames(1-6): A.id, A.length, B.id, B.length, geneName, confidence
	# after 7: read counts
	A.range <- c(colIndexStart.A:colIndexStart.B-1)
	B.range <- c(colIndexStart.B:ncol(df))
  
	df <- df %>%
  	mutate(mean.A=rowMeans(select(., A.range))) %>%
  	mutate(mean.B=rowMeans(select(., B.range))) %>%
  	arrange(desc(.[[6]]))
  
	confidence.counts <- df %>% 
  	filter(.[[6]] == 1) %>%
  	nrow()
	
	df.scbn <- df[,c(2,13,4,14)]
  
  factor <- SCBN::SCBN(orth_gene=df.scbn , hkind=1:confidence.counts , a=0.05)
	scale.factor <- factor$scbn_val
	
	divide_scale <- function(x) {
		return(round(x/scale.factor))
	}
	
	df.count <- df %>% 
		mutate(across(B.range, divide_scale)) %>%
		select(1,3,5,A.range,B.range)
	
	# calculating p-value (sage.test)
	p_value <- SCBN::sageTestNew(x=df.scbn[,2], y=df.scbn[,4],
															 lengthx=df.scbn[,1], lengthy=df.scbn[,3],
															 n1=sum(df.scbn[,2]), n2=sum(df.scbn[,4]),
															 scale=scale.factor)
	df.count <- cbind(df.count, p_value)
	return(list(df.count, scale.factor))
}

#testhmchimp <- scaling_factor(hmchimp,7,10)
#df <- testhmchimp[[1]]
#s <- testhmchimp[[2]]

# combined two speices TE table
scaling_TE <- function(df.A, df.B, scale.factor) {
	divide_scale <- function(x) {
		return(round(x/scale.factor))
	}
	df.B <- df.B %>%
		mutate(across(4:ncol(df.B), divide_scale))
	df <- inner_join(df.A, df.B[,c(1,4:ncol(df.B))], by=colnames(df.A)[1])
	return(df)
}

# merge two TE table
merge_TE <- function(df.A, df.B) {
	df <- inner_join(df.A, df.B[,c(1,7,8,9)], by="name")
	write.csv(df, "data/mergeTE.csv")
	return(df)
}

merge_Ortholog <- function(df.A, df.B) {
	df <- inner_join(df.A, df.B[,c(2,3,8,9,10,11)], by=colnames(df.A)[3])
	df <- df[,c(1,2,12,3,5,6,7,8,9,10,13,14,15,11,16)]
	df <- df[!duplicated(df[,3]),] #remove ncRNA
	write.csv(df, "data/mergeOrtholog.csv")
	return(df)
}

kznfs <- function(ortholog) {
	kznf.annot <- readxl::read_excel("reference/KZNFs423.xls")
	kznf.annot <- kznf.annot[,c(1,13)]
	colnames(kznf.annot) <- c("geneName","chromosome")
	df <- inner_join(ortholog, kznf.annot, by="geneName")
	return(df)
}

corrOrthologTE <- function(ortholog, te) {
	df <- data.frame(matrix(ncol=11, nrow=0))
	colnames(df) <- c("chr", "geneName", "teName", "teFamily", "teClass", "corr.hm", "p.value.hm",
										"corr.chimp", "p.value.chimp", "corr.oran", "p.value.oran")
	for (x in 1:nrow(ortholog)) {
		for (y in 1:nrow(te)) {
			hm.gene <- as.numeric(ortholog[x,c(5,6,7)]) 
			chimp.gene <- as.numeric(ortholog[x,c(8,9,10)])
			oran.gene <- as.numeric(ortholog[x,c(11,12,13)])
			hm.te <- as.numeric(te[y,c(4,5,6)])
			chimp.te <- as.numeric(te[y,c(7,8,9)])
			oran.te <- as.numeric(te[y,c(10,11,12)])
			cor.h <- stats::cor.test(hm.gene, hm.te, method="pearson", alternative="two.sided")
			cor.c <- stats::cor.test(chimp.gene, chimp.te, method="pearson", alternative="two.sided")
			cor.o <- stats::cor.test(oran.gene, oran.te, method="pearson", alternative="two.sided")
			chr <- ortholog$chromosome[x]
			geneName <- ortholog$geneName[x]
			teName <- te$name[y]
			teFamily <- te$family[y]
			teClass <- te$class[y]
			df.temp <- data.frame(chr, geneName, teName, teFamily, teClass, cor.h$p.value, 
														cor.h$estimate, cor.c$p.value, cor.c$estimate, cor.o$p.value, cor.o$estimate)
			df <- rbind(df,df.temp)
		}
	}
	df <- df %>% 
		mutate(padj.hm = p.adjust(cor.h.p.value, method="fdr")) %>%
		mutate(padj.chimp = p.adjust(cor.c.p.value, method="fdr")) %>%
		mutate(padj.oran = p.adjust(cor.o.p.value, method="fdr"))
	return(df)
}

filterCorr <- function(df, p, r) {
	df.all <- df %>% 
		filter(padj.hm<p & padj.chimp<p & padj.oran<p) %>%
		filter(cor.h.estimate<r & cor.c.estimate<r & cor.o.estimate<r)
	df.hm <- df %>% 
		select(1,2,3,4,5,6,7,12) %>%
		filter(padj.hm<p & cor.h.estimate<r)
	df.chimp <- df %>%
		select(1,2,3,4,5,8,9,13) %>%
		filter(padj.chimp<p & cor.c.estimate<r)
	df.oran <- df %>%
		select(1,2,3,4,5,10,11,14) %>%
		filter(padj.oran<p & cor.o.estimate<r)
	return(list(df.all, df.hm, df.chimp, df.oran))
}

deseq2 <- function(countTable) {
	metadata <- data.frame(species=rep(c("human", "chimapnzee", "orangutan"), each=3))
	rownames(metadata) <- c("H1","H2","H3", "C1", "C2", "C3", "O1", "O2", "O3")
	# remove duplicate rows
	countTable <- countTable[!duplicated(countTable[,1]),]
	cts <- countTable[,c(2:10)]
	colnames(cts) <- rownames(metadata)
	rownames(cts) <- countTable[,1]
	dds <- DESeq2::DESeqDataSetFromMatrix(countData=cts, colData=metadata, design=~species)
	dds <- DESeq2::DESeq(dds)
	res <- DESeq2::results(dds, alpha=0.05, lfcThreshold=1)
	summary_res <- DESeq2::summary(res)
	ntd <- DESeq2::normTransform(dds)
	# heatmap
	select <- order(rowMeans(counts(dds, normalized=TRUE)),
									decreasing=TRUE)[1:20]
	df <- as.data.frame(colData(dds)[,"species"])
	rownames(df) <- colnames(ntd)
	heatmap <- pheatmap::pheatmap(assay(ntd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
																cluster_cols=FALSE, annotation_col=df)
	return(list(res, summary_res, heatmap))
}
